
from django.contrib import admin
from django.urls import path, include
from storeapp.views import create_book, delete_book, show_books, show_dabook, update_view
from storeapp.views import (show_mags, create_mag, show_damag, mag_update, delete_mag, show_genre)


urlpatterns = [
    path("books/", show_books, name='show_books'),
    path("create/", create_book, name="create_book"),
    path("<int:pk>/", show_dabook, name="show_dabook"),
    path("<int:pk>/edit/", update_view, name="update_view"),
    path("<int:pk>/delete/", delete_book, name="delete_book"),
### end of book paths ###
    path("mags/", show_mags, name='show_mags'),
    path("createmag/", create_mag, name='create_mag'),
    path("<int:pk>/showmag", show_damag, name='show_damag'),
    path("<int:pk>/editmag/", mag_update, name='mag_update'),
    path("<int:pk>/deletemag/", delete_mag, name='delete_mag'),
    path('genre/<int:pk>/', show_genre, name='show_genre'),
  
]
