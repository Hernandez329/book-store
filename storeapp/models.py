from platform import release
from django.db import models

# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=120)
    author = models.ManyToManyField("Author", related_name="books")
    pages = models.SmallIntegerField(null=True, blank=True)
    isbn = models.BigIntegerField(null=True, blank=True)
    cover = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    publish_year = models.SmallIntegerField(null=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title + " by " + str(self.author.first())

class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

###
class Magazine(models.Model):
    title = models.CharField(max_length=150, unique=True)
    release_cycle = models.CharField(max_length=50, null=True, blank=True)
    description = models.TextField(max_length=5000, null=True, blank=True)
    cover = models.URLField(null=True, blank=True)
    publisher = models.CharField(max_length=100, null=True, blank=True)
    genres = models.ManyToManyField('Genre', related_name='magazines')

    def __str__(self):
        return self.title

class Genre(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

class Issue(models.Model):
    title = models.CharField(max_length=200, unique=True)
    description = models.TextField(max_length=5000, null=True, blank=True)
    date_published = models.DateField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True)
    issue_number = models.SmallIntegerField(null=True)
    coverimg = models.URLField(null=True, blank=True)
    magazine = models.ForeignKey(Magazine, related_name='issues', on_delete=models.CASCADE)

    def _str__(self):
        return str(self.issue_number) + " | " + self.title