from django import forms
from .models import Book, Magazine 

class BookForm(forms.ModelForm):

    class Meta:
        model = Book 

        fields = [
            "title",
            "author",
            "pages",
            "isbn",
            "cover",
            "in_print",
            "publish_year",
            "description",
        ]


class MagForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            'title',
            'release_cycle',
            'description',
            'cover',
            'publisher'
        ]
    