from django.shortcuts import redirect, render, get_object_or_404
from storeapp.forms import BookForm, MagForm
from .models import Book, Magazine, Genre
# Create your views here.


def show_books(request):
    books = Book.objects.all()
    context = {
        'books': books
    }
    return render(request, 'books/list.html', context)

def create_book(request):
    context = {}

    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_books")

    context['form']= form
    return render(request, 'books/create.html', context)   


def show_dabook(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else None,
    }
    return render(request, 'books/details.html', context)

def detail_update(request, pk):
    context = {}

    context['data'] = Book.objects.get(pk=pk)

    return render(request,'books/details.html', context)

def update_view(request, pk):

    context = {}

    obj = get_object_or_404(Book, pk = pk)
    form = BookForm(request.POST or None, instance = obj)
    
    if form.is_valid():
        form.save()
        return redirect('show_books')
    
    context["form"] = form 

    return render(request, "books/edit.html", context)

def delete_book(request, pk):

    context ={}
 
    obj = get_object_or_404(Book, pk=pk)
 
 
    if request.method =="POST":
        
        obj.delete()
       
        return redirect("show_books")
 
    return render(request, "books/delete.html", context)


## Start of the Magazine ###

def show_mags(request):
    mags = Magazine.objects.all()
    context = {
        'mags': mags
    }
    return render(request, 'mags/list.html', context)

def create_mag(request):
    context = {}

    form = MagForm(request.POST or None)
    
    if form.is_valid():
        form.save()
        return redirect("show_mags")
    
    context['form']= form
    return render(request, 'mags/create.html', context)

def show_damag(request, pk):
    context = {
        'mag': Magazine.objects.get(pk=pk) if Magazine else None,
    }
    return render(request, 'mags/details.html', context)

def mag_detail_update(request, pk):
    context = {}

    context['data'] = Magazine.objects.get(pk=pk)

    return render(request, 'mags/details.html', context)

def mag_update(request, pk):
    context = {}

    obj = get_object_or_404(Magazine, pk=pk)
    form = MagForm(request.POST or None, instance = obj)

    if form.is_valid():
        form.save()
        return redirect('show_mags')

    context['form'] = form

    return render(request, 'mags/edit.html', context)

def delete_mag(request, pk):
    
    context = {}

    obj = get_object_or_404(Magazine, pk=pk)

    if request.method =="POST":
        obj.delete()

        return redirect('show_mags')

    return render(request, "mags/delete.html", context)

def show_genre(request, pk):
    genre = Genre.objects.get(pk=pk)

    context = {
        'genre': genre
    }
    return render(request, "mags/genre.html", context)