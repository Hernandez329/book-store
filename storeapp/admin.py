from django.contrib import admin

from .models import Book, Author, BookReview, Magazine, Genre, Issue
# Register your models here.

# class BookAdmin(admin.ModelAdmin):
#     pass

admin.site.register(Book) #Parameters can be (Book, BookAdmin)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Magazine)
admin.site.register(Genre)
admin.site.register(Issue)